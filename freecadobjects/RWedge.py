# From package freecadobjects to be used with python FreeCAD programming.
# Class RBox.
#
# Copyright (C) 2017 Ronja Hetzel <ronja.hetzel@rwth-aachen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.



import FreeCAD 
import Part
import RObject

## class RWedge.
#  RObject using the FreeCAD internal Part::Wedge.
#  Other than in normal FreeCAD, the box is placed with its center in the origin by default.
#  The two xz-planes are parallel, defining base and top.
#  The RObject.referencepoint is the center of the wedge.
class RWedge(RObject.RObject):
	## Constructor.
	#  @param name Name of the new object given as string.
	#  @param doc RDocument to place the object.
	#  @param xmin First x-coordinate of base plane.
	#  @param ymin Y-coordinate of base plane.
	#  @param zmin First z-coordinate of base plane.
	#  @param x2min First x-coordinate of top plane.
	#  @param z2min First z-coordinate of top plane.
	#  @param xmax Second x-coordinate of base plane.
	#  @param ymax Y-coordinate of base plane.
	#  @param zmax Second z-coordinate of top plane.
	#  @param x2max Second x-coordinate of top plane.
	#  @param z2max Second z-coordinate of top plane.
	def __init__(self,name,doc,xmin,ymin,zmin,x2min,z2min,xmax,ymax,zmax,x2max,z2max):
		RObject.RObject.__init__(self,name,doc)
		## @see RObject.geo
		self.geo = self.doc.page.addObject("Part::Wedge",name)
		## @see RObject.vis
		self.vis = self.doc.gui.getObject(name)
		self.geo.Xmin = xmin
		self.geo.Ymin = ymin
		self.geo.Zmin = zmin
		self.geo.X2min = x2min
		self.geo.Z2min = z2min
		self.geo.Xmax = xmax
		self.geo.Ymax = ymax
		self.geo.Zmax = zmax
		self.geo.X2max = x2max
		self.geo.Z2max = z2max
		self.place()
		shift = FreeCAD.Vector((xmin+xmax+x2min+x2max)/4.,(ymin+ymax)/2.,(zmin+zmax+z2min+z2max)/4.)
		self.changeinitialposition(shift)

	## Copy this wedge to get a new object with the same properties.
	#  @param newname Name of the new object given as string.
	def copy(self,newname):
		newobject = RWedge(newname,self.doc,self.geo.Xmin,self.geo.Ymin,self.geo.Zmin,self.geo.X2min,self.geo.Z2min,self.geo.Xmax,self.geo.Ymax,self.geo.Zmax,self.geo.X2max,self.geo.Z2max)
		self.copyattributes(newobject)
		return newobject
    

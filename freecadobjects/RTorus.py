# From package freecadobjects to be used with python FreeCAD programming.
# Class RTorus.
#
# Copyright (C) 2017 Ronja Hetzel <ronja.hetzel@rwth-aachen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.



import FreeCAD 
import Part
import RObject

## class RTorus.
#  RObject using the FreeCAD internal Part::Torus.
#  The torus is not filled, but realised as a hollow closed tube.
#  Parts of the surface of this tube can be removed by changing 
#  the values of RTorus.Angle1, RTorus.Angle2 and RTorus.Angle3.
#  In the inital position the center of the torus is in the origin.
#  The RObject.referencepoint is the center of the torus.
class RTorus(RObject.RObject):
	## Constructor.
	#  @param name Name of the new object given as string.
	#  @param doc RDocument to place the object.
	#  @param r1 Radius from the center of the torus to the center of tube.
	#  @param r2 Radius of the tube to form the ring.
	def __init__(self,name,doc,r1,r2):
		RObject.RObject.__init__(self,name,doc)
		## @see RObject.geo
		self.geo = self.doc.page.addObject("Part::Torus",name)
		## @see RObject.vis
		self.vis = self.doc.gui.getObject(name)
		self.geo.Radius1 = r1
		self.geo.Radius2 = r2
		self.place()

	## Copy this torus to get a new object with the same properties.
	#  @param newname Name of the new object given as string.
	def copy(self,newname):
		newobject = RTorus(newname,self.doc,self.geo.Radius1,self.geo.Radius2)
		self.copyattributes(newobject)
		return newobject
 

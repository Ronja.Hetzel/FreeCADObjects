# From package freecadobjects to be used with python FreeCAD programming.
# Class RCylinder.
#
# Copyright (C) 2017 Ronja Hetzel <ronja.hetzel@rwth-aachen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.



import FreeCAD 
import Part
import RObject

## class RCylinder.
#  RObject using the FreeCAD internal Part::Cylinder.
#  Other than in normal FreeCAD, the cylinder is placed with its center in the origin by default.
#  The cylinders' symmetry axis points into z-direction when constructed.
#  The RObject.referencepoint is the center of the cylinder.
class RCylinder(RObject.RObject):
	## Constructor.
	#  @param name Name of the new object given as string.
	#  @param doc RDocument to place the object.
	#  @param r Raduis of the cylinder.
	#  @param l Length of the cylinder.
	def __init__(self,name,doc,r,l):
		RObject.RObject.__init__(self,name,doc)
		## @see RObject.geo
		self.geo = self.doc.page.addObject("Part::Cylinder",name)
		self.geo.Radius = r
		self.geo.Height = l
		## @see RObject.vis
		self.vis = self.doc.gui.getObject(name)
		self.place()
		shift = FreeCAD.Vector(0.0,0.0,l/2.)
		self.changeinitialposition(shift)

	## Copy this cylinder to get a new object with the same properties.
	#  @param newname Name of the new object given as string.
	def copy(self,newname):
		newobject = RCylinder(newname,self.doc,self.geo.Radius,self.geo.Height)
		self.copyattributes(newobject)
		return newobject
    

# From package freecadobjects to be used with python FreeCAD programming.
# Class RDimension.
#
# Copyright (C) 2017 Ronja Hetzel <ronja.hetzel@rwth-aachen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.



import FreeCAD 

## class RDimension.
#  Unknown, if it is working. I know I tried working with it, but I don't remember with what outcome.
#  @todo Check if RDimension is working.
class RDimension():
	## Constructor.
	#  @param doc RDocument to measure the dimension in.
	#  @param p1 Point 1 given to Draft.makeDimension.
	#  @param p2 Point 2 given to Draft.makeDimension.
	#  @param p3 Point 3 given to Draft.makeDimension.
	def __init__(self,doc,p1,p2,p3):
		self.doc = doc
		self.label = "Dimension"
		global dimensionscounter
		if dimensionscounter>=1 and dimensionscounter<10: 
			self.label = self.label + "00" + str(dimensionscounter)
		if dimensionscounter>=10 and dimensionscounter<100: 
			self.label = self.label + "0" + str(dimensionscounter)
		if dimensionscounter>=100:
			print("I cannot handle so many dimensions!!!")
		self.geo = Draft.makeDimension(p1,p2,p3)
		## @see RObject.vis
		self.vis = self.doc.gui.getObject(self.label)
		dimensionscounter += 1
		self.doc.recompute()
		self.vis.DisplayMode = "3D"

	## Set the font size of the label giving the length of the measured dimension.
	#  Try for example with font size 50.
	def setfontsize(self,fontsize):
		self.vis.FontSize = fontsize


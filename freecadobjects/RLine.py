# From package freecadobjects to be used with python FreeCAD programming.
# Class RPlane.
#
# Copyright (C) 2017 Ronja Hetzel <ronja.hetzel@rwth-aachen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.



import FreeCAD 
import Part
import RObject

## class RLine.
#  RObject using the FreeCAD internal Part::Line.
#  Draw a line between the tow points (x1,y1,z1) and (x2,y2,z2).
#  The RObject.referencepoint is (x1,y1,z1).
class RLine(RObject.RObject):
	## Constructor.
	#  @param name Name of the new object given as string.
	#  @param doc RDocument to place the object.
	#  @param x1 X-coordinate of first point.
	#  @param y1 Y-coordinate of first point.
	#  @param z1 Z-coordinate of first point.
	#  @param x2 X-coordinate of second point.
	#  @param y2 Y-coordinate of second point.
	#  @param z2 Z-coordinate of second point.
	def __init__(self,name,doc,x1,y1,z1,x2,y2,z2):
		RObject.RObject.__init__(self,name,doc)
		## @see RObject.geo
		self.geo = self.doc.page.addObject("Part::Line",name)
		## @see RObject.vis
		self.vis = self.doc.gui.getObject(name)
		self.geo.X1 = x1
		self.geo.Y1 = y1
		self.geo.Z1 = z1
		self.geo.X2 = x2
		self.geo.Y2 = y2
		self.geo.Z2 = z2
		self.place()

	## Set width of line.
	#  @param linewidth Width of line. Default is 2.
	def setlinewidth(self,linewidth):
		self.vis.LineWidth = linewidth

	## Define the color of a RLine.
	#  @param color Has to be given in the form (r,g,b). Specifies the red, green and blue fraction of the color. Each of the three values has to be between 0 and 1.
	def setcolor(self,color):
		self.vis.LineColor = color

	## Copy this line to get a new object with the same properties.
	#  @param newname Name of the new object given as string.
	def copy(self,newname):
		newobject = RLine(newname,self.doc,self.geo.X1,self.geo.Y1,self.geo.Z1,self.geo.X2,self.geo.Y2,self.geo.Z2)
		self.copyattributes(newobject)
		newobject.setlinewidth(self.vis.LineWidth)
		return newobject
    

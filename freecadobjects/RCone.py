# From package freecadobjects to be used with python FreeCAD programming.
# Class RCone.
#
# Copyright (C) 2017 Ronja Hetzel <ronja.hetzel@rwth-aachen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.



import FreeCAD 
import Part
import RBasics
import RObject

## class RCone.
#  RObject using the FreeCAD internal Part::Cone.
#  In the inital position the base is in the xy-plane and the symmetry axis is pointing in z-direction.
#  The RObject.referencepoint is the center of the base circle.
class RCone(RObject.RObject):
	## Constructor.
	#  @param name Name of the new object given as string.
	#  @param doc RDocument to place the object.
	#  @param rb Radius of the cone at the base.
	#  @param rt Radius of the cone at the top. If it is set to 0, it results in a normal cone, otherwise one gets a truncated cone.
	#  @param h Height of the cone.
	def __init__(self,name,doc,rb,rt,h):
		RObject.RObject.__init__(self,name,doc)
		## @see RObject.geo
		self.geo = self.doc.page.addObject("Part::Cone",name)
		## @see RObject.vis
		self.vis = self.doc.gui.getObject(name)
		self.geo.Radius1 = rb
		self.geo.Radius2 = rt
		self.geo.Height = h
		self.place()

	## Returns the position of the tip of the cone. If the cone is trucated, it returns the center of the circle forming the top.
	def getTipPosition(self):
		helpaxis = FreeCAD.Vector(0,0,self.geo.Height)
		rotatedaxis = RBasics.rotateVectorAroundOrigin(helpaxis,self.rotation.Axis,self.rotation.Angle)	
		return self.translation+rotatedaxis

	## Copy this cone to get a new object with the same properties.
	#  @param newname Name of the new object given as string.
	def copy(self,newname):
		newobject = RCone(newname,self.doc,self.geo.Radius1,self.geo.Radius2,self.geo.Height)
		self.copyattributes(newobject)
		return newobject
 

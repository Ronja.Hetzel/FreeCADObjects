# From package freecadobjects to be used with python FreeCAD programming.
# Class RFuse.
#
# Copyright (C) 2017 Ronja Hetzel <ronja.hetzel@rwth-aachen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.



import FreeCAD 
import Part
import RCompound

## class RFuse.
#  RObject combined from two existing RObjects.
class RFuse(RCompound.RCompound):
	## Constructor.
	#  @param name Name of the new object given as string.
	#  @param obj1 First RObject that will be part of the compound object. The compound object will inherit the RObject.referencepoint of this object.
	#  @param obj2 Second RObject that will be part of the compound object.
	def __init__(self,name,obj1,obj2):
		RCompound.RCompound.__init__(self,name,obj1,obj2)
		## @see RObject.geo
		self.geo = self.doc.page.addObject("Part::Fuse",name)
		self.geo.Base = obj1.geo
		self.geo.Tool = obj2.geo
		## @see RObject.vis
		self.vis = self.doc.gui.getObject(name)
		self.place()

	## Copy this compound to get a new object with the same properties.
	#  Both parts for the compound are copied individually and then a new compund is created.
	#  @param newname Name of the new object given as string.
	def copy(self,newname):
		newobj1 = self.object1.copy(self.object1.name+"_copy")
		newobj2 = self.object2.copy(self.object2.name+"_copy")
		newfuse = RFuse(newname,newobj1,newobj2)
		self.copyattributes(newfuse)
		return newfuse

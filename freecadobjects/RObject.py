# From package freecadobjects to be used with python FreeCAD programming.
# Class RObject.
#
# Copyright (C) 2017 Ronja Hetzel <ronja.hetzel@rwth-aachen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.



import FreeCAD 
import RBasics

## virtual class RObject.
#  No standalone RObject should be constructed, but 
#  all objects inherit common functionality.
class RObject():
	## Constructor.
	#  Is called explicitly when a new object inheriting from RObject is generated.
	#  @param name Name of the new object given as string.
	#  @param document RDocument to place the object.
	def __init__(self,name,document):
		## Name of the object given as string.
		self.name = name
		## RDocument in which the object is placed.
		self.doc = document
		## Translation of the object as it is used internally by FreeCAD. Given as FreeCAD.Rotation.
		self.translation = RBasics.origin
		## Rotation of the object as it is used internally by FreeCAD. Given as FreeCAD.Vector.
		self.rotation = RBasics.defaultorientation
		## Position of a referencepoint of the object within the FreeCAD coordinate system. Check the specific object classes to see what point it refers to in this object. Given as FreeCAD.Vector.
		self.referencepoint = RBasics.origin
		## Pointer to the FreeCAD geometry representation of the object.
		self.geo = 0
		## Pointer to the FreeCAD visualisation of the object.
		self.vis = 0 

	## If the default translation FreeCAD assignes to an object is not identical with the desired RObject.referencepoint of the RObject, the RObjedt has to be shifted by this offset in its constructor using this function.
	def changeinitialposition(self,shift):
		storereferenceposition = self.translation
		self.translate(self.translation-shift)
		self.referencepoint = storereferenceposition

	## Function to place a newly created RObject into the document. The document is recomputed, which is necessary to get the visualisation of the RObject right.
	def place(self):
		self.geo.Placement = FreeCAD.Placement(self.translation,self.rotation)
		self.doc.recompute()

	## Define the color of a RObject.
	#  @param color Has to be given in the form (r,g,b). Specifies the red, green and blue fraction of the color. Each of the three values has to be between 0 and 1.
	def setcolor(self,color):
		self.vis.ShapeColor = color

	## Define the transparency of a RObject.
	#  @param t The transparency level. Given as percentage value between 0 (solid) and 100 (fully transparent).
	def settransparency(self,t):
		self.vis.Transparency = t

	## Shift the RObject by a given vector.
	#  @param translationvector FreeCAD.Vector to describe the translation. 
	def translate(self,translationvector):
		self.internaltranslation(translationvector)
		self.geo.Placement = FreeCAD.Placement(self.translation,self.rotation)

	## Place the RObject on a specific position, which refers to the FreeCAD internal Translation.
	#  @param position FreeCAD.Vector to describe the translation. 
	def settranslation(self,position):
		self.translate(position-self.translation)

	## Move the RObject.referencepoint to a specific position.
	#  @param position FreeCAD.Vector to describe the new position of the RObject.referencepoint. 
	def setposition(self,position):
		self.translate(position-self.referencepoint)

	## Shift the RObject by a given vector.
	#  @param translationvector FreeCAD.Vector to describe the translation. 
	def internaltranslation(self,translationvector):
		self.translation = self.translation + translationvector
		self.referencepoint = self.referencepoint + translationvector

	## Rotate object by a given angle around a given axis.
	#  @param rotationvector FreeCAD.Rotation(FreeCAD.Vector(x,y,z),angle) angle given in degree, internally converted to rad.
	#  @todo Take care of compound objects.
	def rotate(self,rotationvector):
		self.setrotation(RBasics.combineRotations(self.rotation,rotationvector))

 	## Set the FreeCAD rotation of the RObject. 
 	#  @param rotationvector FreeCAD.Rotation(FreeCAD.Vector(x,y,z),angle) angle given in degree, internally converted to rad.
	#  @todo Separate implementation of the function setrotation for compound shapes. It is necessary to update the internal coordinates of the basic shapes forming the object, if they should be used in later placements or calculations. If just the compound object is addressed, it already works well.
	def setrotation(self,rotationvector):
		#FreeCAD.Console.PrintMessage(self.name)
		#FreeCAD.Console.PrintMessage(": Set Rotation\n")
		#FreeCAD.Console.PrintMessage(rotationvector.Axis)
		#FreeCAD.Console.PrintMessage(", ")
		#FreeCAD.Console.PrintMessage(rotationvector.Angle*180./3.1415)
		#FreeCAD.Console.PrintMessage("\n")
		self.geo.Placement = FreeCAD.Placement(self.translation,rotationvector)
		refpostranslationvector = self.internalrotation(rotationvector)
		self.translate(refpostranslationvector)
		
	## Set the internal vector when a new rotation is set for a RObject.
	#  @param rotationvector FreeCAD.Vector to describe the rotation. 
	def internalrotation(self,rotationvector):
		self.rotation = rotationvector
		rotangle = rotationvector.Angle
		rotaxis = rotationvector.Axis
		newrefpos = RBasics.rotateVector(self.referencepoint,self.translation,rotaxis,rotangle)
		refpostranslationvector = self.referencepoint - newrefpos
		self.referencepoint = newrefpos
		return refpostranslationvector

	## Copy all common attributes to a newly created object.
	#  This function is called by the individual RObjects to propagate their attributes (referencepoint, position, rotation, color, transparency) to the new object when they are copied.
	#  @param newobject The new RObject which gets the attributes.
	def copyattributes(self,newobject):
		newobject.referencepoint = self.referencepoint
		newobject.setrotation(self.rotation)
		newobject.settranslation(self.translation)
		(r,g,b,t) = self.vis.ShapeColor
		newobject.setcolor((r,g,b))
		newobject.settransparency(self.vis.Transparency)

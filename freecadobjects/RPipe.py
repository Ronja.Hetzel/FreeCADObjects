# From package freecadobjects to be used with python FreeCAD programming.
# Class RPipe.
#
# Copyright (C) 2017 Ronja Hetzel <ronja.hetzel@rwth-aachen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.



import FreeCAD 
import Part
import RCut
import RCylinder

## class RPipe.
#  RObject combined from two concentric RCylinder s. 
#  The inner one is cut out of the outer one to form a pipe.
#  The RObject.referencepoint is in the center of the pipe. 
class RPipe(RCut.RCut):
	## Constructor.
	#  @param name Name of the new object given as string.
	#  @param doc RDocument to place the object.
	#  @param r1 Inner radius of the pipe. Has to be smaller than r2.
	#  @param r2 Outer radius of the pipe. Has to be larger than r1.
	#  @param l Length of the pipe.
	def __init__(self,name,doc,r1,r2,l):
		self.innerradius = r1
		self.outerradius = r2
		self.length = l
		name1 = name + "_Cylinder1"
		name2 = name + "_Cylinder2"
		cy1 = RCylinder.RCylinder(name1,doc,r1,l)
		cy2 = RCylinder.RCylinder(name2,doc,r2,l)
		RCut.RCut.__init__(self,name,cy2,cy1)
		self.place()


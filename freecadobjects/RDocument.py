# From package freecadobjects to be used with python FreeCAD programming.
# Class RDocument.
#
# Copyright (C) 2017 Ronja Hetzel <ronja.hetzel@rwth-aachen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.



import FreeCAD 
import FreeCADGui

## class RDocument.
#  Creates a FreeCAD Document and provides a reference to it.
class RDocument():
        ## Constructor.
        #  @param name Name of the new document given as string.
	def __init__(self,name):
                ## Name of the document you need to refer to it from within FreeCAD.
		self.docname = name
		FreeCAD.newDocument(self.docname)
		FreeCAD.setActiveDocument(self.docname)
                ## Reference to the actual FreeCAD document.
		self.page=FreeCAD.getDocument(self.docname)
                ## Reference of the FreeCADGui / the visualisation of the document. 
		self.gui=FreeCADGui.getDocument(self.docname)

        ## Recompute the document to update objects, colors, visalisition...
	def recompute(self):	
		self.page.recompute()


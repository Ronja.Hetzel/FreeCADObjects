# From package freecadobjects to be used with python FreeCAD programming.
# Package definition.
#
# Copyright (C) 2017 Ronja Hetzel <ronja.hetzel@rwth-aachen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.



## @package freecadobjects
#  Package freecadobjects
#
#  Makes programming FreeCAD with python easier as 
#  long as you just want to construct your setup out 
#  of simple shapes available as Part objects.
from RBox import RBox
from RCone import RCone
from RCut import RCut
from RCylinder import RCylinder
from RDimension import RDimension
from RDocument import RDocument
from RFeaturePage import RFeaturePage
from RFuse import RFuse
from RLine import RLine
from RPipe import RPipe
from RPlane import RPlane
from RSphere import RSphere
from RTorus import RTorus
from RView import RView
from RWedge import RWedge

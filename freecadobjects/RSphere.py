# From package freecadobjects to be used with python FreeCAD programming.
# Class RSphere.
#
# Copyright (C) 2017 Ronja Hetzel <ronja.hetzel@rwth-aachen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.



import FreeCAD 
import Part
import RObject

## class RSphere.
#  RObject using the FreeCAD internal Part::Sphere.
#  In the inital position the center of the sphere is in the origin.
#  The RObject.referencepoint is the center of the sphere.
class RSphere(RObject.RObject):
	## Constructor.
	#  @param name Name of the new object given as string.
	#  @param doc RDocument to place the object.
	#  @param r Radius of the sphere.
	def __init__(self,name,doc,r):
		RObject.RObject.__init__(self,name,doc)
		## @see RObject.geo
		self.geo = self.doc.page.addObject("Part::Sphere",name)
		## @see RObject.vis
		self.vis = self.doc.gui.getObject(name)
		self.geo.Radius = r
		self.place()

	## Copy this sphere to get a new object with the same properties.
	#  @param newname Name of the new object given as string.
	def copy(self,newname):
		newobject = RSphere(newname,self.doc,self.geo.Radius)
		self.copyattributes(newobject)
		return newobject
 

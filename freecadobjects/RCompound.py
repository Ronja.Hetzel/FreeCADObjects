# From package freecadobjects to be used with python FreeCAD programming.
# Class RCompound.
#
# Copyright (C) 2017 Ronja Hetzel <ronja.hetzel@rwth-aachen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.



import FreeCAD 
import RBasics
import RObject

## virtual class RCompound.
#  No standalone RCompound should be constructed, but 
#  all compound objects inherit common functionality.
#  Used to form a new object out of two existing ones (fusion, subtraction).
class RCompound(RObject.RObject):
	## Constructor.
	#  @param name Name of the new object given as string.
	#  @param obj1 First RObject that will be part of the compound object. The compound object will inherit the RObject.referencepoint of this object.
	#  @param obj2 Second RObject that will be part of the compound object.
	def __init__(self,name,obj1,obj2):
		self.name = name
		self.doc = obj1.doc
		self.rotation = RBasics.defaultorientation
		self.translation = RBasics.origin
		self.referencepoint = obj1.referencepoint
		self.object1 = obj1
		self.object2 = obj2

	## Define the color of both components of the RCompound.
	#  @param color Has to be given in the form (r,g,b). Specifies the red, green and blue fraction of the color. Each of the three values has to be between 0 and 1.
	def setcolor(self,color):
		self.vis.ShapeColor = color
		self.object1.setcolor(color)
		self.object2.setcolor(color)

	## Define the transparency of a RObject.
	#  @param t The transparency level. Given as percentage value between 0 (solid) and 100 (fully transparent).
	def settransparency(self,t):
		self.vis.Transparency = t
		self.object1.settransparency(t)
		self.object2.settransparency(t)

	## Overwrite for function copyattributes from RObject.
	#  For compounds only referencepoint, position and rotation are copied to a new object.
	#  Color and transparency are copied from the basic objects when the copy command goes through all compound stages iteratively.
	#  Coping them again for compounds leads to all grey objects.
	#  @param newobject The new RObject which gets the attributes.
	def copyattributes(self,newobject):
		newobject.referencepoint = self.referencepoint
		newobject.settranslation(self.translation)
		newobject.setrotation(self.rotation)

#TEST   ## Set the internal parameters when shifting the RCompound and its components by a given vector.
#TEST   #  @param translationvector FreeCAD.Vector to describe the translation. 
#TEST	def translate(self,translationvector):
#TEST		RObject.RObject.translate(self,translationvector)
#TEST		self.object1.internaltranslation(translationvector)
#TEST		self.object2.internaltranslation(translationvector)
#TEST
#TEST	def rotate(self,rotationvector):
#TEST		RObject.RObject.rotate(self,rotationvector)
#TEST		t1 = self.object1.internalrotation(RBasics.combineRotations(self.rotation,rotationvector))
#TEST		t2 = self.object2.internalrotation(RBasics.combineRotations(self.rotation,rotationvector))
#TEST		self.object1.internaltranslation(t1)
#TEST		self.object2.internaltranslation(t2)
#
#        #  @todo lines for obj 1 and 2 can not work
#	def setrotation(self,rotationvector):
#		RObject.RObject.setrotation(self,rotationvector)
#		#self.object1.internalrotation(rotationvector)
#		#self.object2.internalrotation(rotationvector)

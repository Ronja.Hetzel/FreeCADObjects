# From package freecadobjects to be used with python FreeCAD programming.
# Class RFeaturePage.
#
# Copyright (C) 2017 Ronja Hetzel <ronja.hetzel@rwth-aachen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.



import FreeCAD 
import Drawing

## class RFeaturePage.
#  Crates a worksheet to in the style of a technical drawing.
#  Place one or several RView on this page to show your construction.
class RFeaturePage():
	## Constructor.
	#  @param name Name of the new object given as string.
	#  @param document RDocument to place the object.
	#  @param form Format given as string. Eg "A3_Landscape".
	def __init__(self,name,document,form):
		## Name of the object given as string.
		self.name = name
		## RDocument in which the object is placed.
		self.doc = document
		## The FreeCAD internal feature page.
		self.worksheet = self.doc.page.addObject('Drawing::FeaturePage',name)
		self.setformat(form)
		self.doc.recompute()
		self.seteditabletexts("","","","","","","","","","")
		## The format of the text in the information box.
		#  The default is "utf-8". If you want to have something else, 
		#  set the value after initialisation and before calling RFeaturePage.fillinfobox().
		self.textformat = "utf-8"

	## Set format of page.
	#  @param form Format given as string. Eg "A3_Landscape".
	#  @todo Include check if given string refers to a valid format.
	def setformat(self,form):
		self.worksheet.Template = FreeCAD.getResourceDir()+'Mod/Drawing/Templates/'+form+'.svg'

	## Place text in the info box at the bottom of the feature page.
	#  The texts have to be saved in the internal variables 
	#  RFeaturePage.textauthor, RFeaturePage.textcreationdate, RFeaturePage.textsupervisor, RFeaturePage.textcheckdate, RFeaturePage.textscale, RFeaturePage.textweight, RFeaturePage.textnumber, RFeaturePage.textsheet, RFeaturePage.texttitle and RFeaturePage.textsubtitle
	#  before.
	#  You can do this separately for all values or use RFeaturePage.seteditabletexts() to set them all at once.
	#  If you do not want to set the text in the default format, change the variable
	#  RFeaturePage.textformat before calling this function. @see RFeaturePage.textformat
	def fillinfobox(self):
		self.worksheet.EditableTexts = [unicode(self.textauthor,self.textformat),unicode(self.textcreationdate,self.textformat),unicode(self.textsupervisor, self.textformat),unicode(self.textcheckdate, self.textformat),unicode(self.textscale, self.textformat),unicode(self.textweight, self.textformat),unicode(self.textnumber, self.textformat),unicode(self.textsheet, self.textformat),unicode(self.texttitle, self.textformat),unicode(self.textsubtitle, self.textformat),]
	
	## Set the texts for the info box.
	#  This functions just saves the values internally. To place them in the info box, call
	#  RFeaturePage.fillinfobox()
	#  If you want to leave some of the fields free, just pass an empty string.
	#  @param author See RFeaturePage.textauthor.
	#  @param creationdate See RFeaturePage.textcreationdate.
	#  @param supervisor See RFeaturePage.textsupervisor.
	#  @param checkdate See RFeaturePage.textcheckdate.
	#  @param scale See RFeaturePage.textscale.
	#  @param weight See RFeaturePage.textweight.
	#  @param number See RFeaturePage.textnumber.
	#  @param sheet See RFeaturePage.textsheet.
	#  @param title See RFeaturePage.texttitle.
	#  @param subtitle See RFeaturePage.textsubtitle.
	def seteditabletexts(self,author,creationdate,supervisor,checkdate,scale,weight,number,sheet,title,subtitle):
		## Part of the info box at the bottom of the feature page. Given as string.
		#  The name of the author/designer of the construction.
		self.textauthor = author
		## Part of the info box at the bottom of the feature page. Given as string.
		#  The date when this construction was made.
		self.textcreationdate = creationdate
		## Part of the info box at the bottom of the feature page. Given as string.
		#  Name of the supervisor who approved this work.
		self.textsupervisor = supervisor
		## Part of the info box at the bottom of the feature page. Given as string.
		#  Date when the construction was checked and approved by the supervisor.
		self.textcheckdate = checkdate
		## Part of the info box at the bottom of the feature page. Given as string.
		#  Scale of the drawing (e.g. 1:10).
		self.textscale = scale
		## Part of the info box at the bottom of the feature page. Given as string.
		#  Weight of the construction in kg.
		self.textweight = weight
		## Part of the info box at the bottom of the feature page. Given as string.
		#  Version number / drawing number of the construction.
		self.textnumber = number
		## Part of the info box at the bottom of the feature page. Given as string.
		#  If there are several sheets for this construction, give the number (e.g. "2 of 3" or "2/3").
		self.textsheet = sheet
		## Part of the info box at the bottom of the feature page. Given as string.
		#  Meaningful title of the document.
		self.texttitle = title
		## Part of the info box at the bottom of the feature page. Given as string.
		#  Subtitle or closer description of the document.
		self.textsubtitle = subtitle


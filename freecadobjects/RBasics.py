# From package freecadobjects to be used with python FreeCAD programming.
# Definitions of common functions.
#
# Copyright (C) 2017 Ronja Hetzel <ronja.hetzel@rwth-aachen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.



import FreeCAD 
import math


## Origin of the FreeCAD coordinate system given as FreeCAD.Vector.
origin = FreeCAD.Vector(0.0,0.0,0.0)
## Normalised vector in x-direction of the FreeCAD coordinate system given as FreeCAD.Vector.
xaxis = FreeCAD.Vector(1.0,0.0,0.0)
## Normalised vector in y-direction of the FreeCAD coordinate system given as FreeCAD.Vector.
yaxis = FreeCAD.Vector(0.0,1.0,0.0)
## Normalised vector in z-direction of the FreeCAD coordinate system given as FreeCAD.Vector.
zaxis = FreeCAD.Vector(0.0,0.0,1.0)
## The rotation FreeCAD assignes to all newly created objects. It points in z-direction with the rotation angle set to zero degree. Given as FreeCAD.Rotation.
defaultorientation = FreeCAD.Rotation(zaxis,0) # z-axis, 0 degree


## Function to rotate a vector around the origin of the FreeCAD coordinate system.
#  @param vector FreeCAD.Vector to be rotated.
#  @param axis Axis to rotate around. Given as FreeCAD.Vector.
#  @param angle Angle given in degree.
#  @return The rotated FreeCAD.Vector.
def rotateVectorAroundOrigin(vector,axis,angle):
	axis = axis.normalize()
	u,v,w = axis.x,axis.y,axis.z
	x,y,z = vector.x,vector.y,vector.z
	#cos = math.cos(math.radians(angle))	# for angle in degree
	#sin = math.sin(math.radians(angle)) 
	cos = math.cos(angle)			# angle is converted to pi-values by FreeCAD.Rotation 
	sin = math.sin(angle)
	temp = (u*x+v*y+w*z)*(1-cos)
	newx = u*temp + x*cos + (-w*y+v*z)*sin
	newy = v*temp + y*cos + (w*x-u*z)*sin
	newz = w*temp + z*cos + (-v*x+u*y)*sin
	newvector = FreeCAD.Vector(newx,newy,newz)
	return newvector


## Function to rotate a vector.
#  @param vector FreeCAD.Vector to be rotated.
#  @param center Point to rotate around. Given as FreeCAD.Vector.
#  @param axis Axis to rotate around. Given as FreeCAD.Vector.
#  @param angle Angle given in degree.
#  @return The rotated FreeCAD.Vector.
def rotateVector(vector,center,axis,angle):
	#plus_center = center
	#minus_center = origin-center
	vector = vector.sub(center)
	vector = rotateVectorAroundOrigin(vector,axis,angle)
	vector = vector.add(center)
	return vector

## Calculate the resulting rotation if two rotations are performed one after the other 
#  @param rotation1 Rotation with is performed first. Given as FreeCAD.Rotation
#  @param rotation2 Rotation with is performed after the first one. Given as FreeCAD.Rotation
#  @return Resulting rotation given as FreeCAD.Rotation
def combineRotations(rotation1,rotation2):
	n1 = rotation1.Axis
	n1.normalize()
	a1 = rotation1.Angle
	n2 = rotation2.Axis
	n2.normalize()
	a2 = rotation2.Angle
	sina1 = math.sin(a1/2.)
	sina2 = math.sin(a2/2.)
	cosa1 = math.cos(a1/2.)
	cosa2 = math.cos(a2/2.)
	cross = n2.cross(n1)
	b = 2*math.acos(cosa1*cosa2-sina1*sina2*n2.dot(n1))
	m = n2.multiply(sina2*cosa1)+n1.multiply(cosa2*sina1)+cross.multiply(sina1*sina2)
	#m.multiply(1./math.sin(b/2.))
	m.normalize()
	return FreeCAD.Rotation(m,b*180/math.pi)


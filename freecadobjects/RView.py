# From package freecadobjects to be used with python FreeCAD programming.
# Class RView.
#
# Copyright (C) 2017 Ronja Hetzel <ronja.hetzel@rwth-aachen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.



import FreeCAD 
import Drawing

## class RView.
#  A technical view of your construction to be placed in a RFeaturePage().
#  You have to create a RFeaturePage() before you can place a RView().
class RView():
        ## Constructor.
        #  @param name Name of the view. Given as string.
        #  @param featurepage The RFeaturePage() to place the view.
        #  @param obj The RObject() you want to display.
        #  @param direction Direction of the projection. Given as tuple of the three vector componants.
        #  @param x X-Position of the RView() on the RFeaturePage() in modelling units (mm).
        #  @param y Y-Position of the RView() on the RFeaturePage() in modeling units (mm).
        #  @param scale Scale the size of the view. Try for example 0.25.
        #  @param rotation Orientation angle of the view on the feature page. Given in degree.
	def __init__(self,name,featurepage,obj,direction,x,y,scale,rotation):
                ## FreeCAD referece to the created view object.
		self.view = featurepage.doc.page.addObject('Drawing::FeatureViewPart',name)
		self.view.Source = obj.geo
		self.view.Direction = direction
		self.view.X = x
		self.view.Y = y
		self.view.Scale = scale
		self.view.Rotation = rotation
		self.view.ShowHiddenLines = True
		featurepage.worksheet.addObject(self.view) 


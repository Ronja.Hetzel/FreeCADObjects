# From package freecadobjects to be used with python FreeCAD programming.
# Class RCut.
#
# Copyright (C) 2017 Ronja Hetzel <ronja.hetzel@rwth-aachen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.



import FreeCAD 
import Part
import RCompound

## class RCut.
#  RObject build out of one existing RObjects where the volume of a second RObjects is cut out.
class RCut(RCompound.RCompound):
	## Constructor.
	#  @param name Name of the new object given as string.
	#  @param obj RObject that will stay part of the compound object. The compound object will inherit the RObject.referencepoint of this object.
	#  @param objhole RObject that will be cut from the first one.
	def __init__(self,name,obj,objhole):
		RCompound.RCompound.__init__(self,name,obj,objhole)
		## @see RObject.geo
		self.geo = self.doc.page.addObject("Part::Cut",name)
		self.geo.Base = obj.geo
		self.geo.Tool = objhole.geo
		## @see RObject.vis
		self.vis = self.doc.gui.getObject(name)
		self.place()

	## Copy this compound to get a new object with the same properties.
	#  Both parts for the compound are copied individually and then a new compund is created.
	#  @param newname Name of the new object given as string.
	def copy(self,newname):
		newobj1 = self.object1.copy(self.object1.name+"_copy")
		newobj2 = self.object2.copy(self.object2.name+"_copy")
		newcut = RCut(newname,newobj1,newobj2)
		self.copyattributes(newcut)
		return newcut

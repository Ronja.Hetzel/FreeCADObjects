# From package freecadobjects to be used with python FreeCAD programming.
# Class RBox.
#
# Copyright (C) 2017 Ronja Hetzel <ronja.hetzel@rwth-aachen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.



import FreeCAD 
import Part
import RObject

## class RBox.
#  RObject using the FreeCAD internal Part::Box.
#  Other than in normal FreeCAD, the box is placed with its center in the origin by default.
#  The RObject.referencepoint is the center of the box.
class RBox(RObject.RObject):
	## Constructor.
	#  @param name Name of the new object given as string.
	#  @param doc RDocument to place the object.
	#  @param l Length of the box in x-direction.
	#  @param w Length of the box in y-direction.
	#  @param h Length of the box in z-direction.
	def __init__(self,name,doc,l,w,h):
		RObject.RObject.__init__(self,name,doc)
		## @see RObject.geo
		self.geo = self.doc.page.addObject("Part::Box",name)
		## @see RObject.vis
		self.vis = self.doc.gui.getObject(name)
		self.setsize(l,w,h)
		self.place()
		shift = FreeCAD.Vector(l/2.,w/2.,h/2.)
		self.changeinitialposition(shift)

	## Define the size of the box.
	#  @param l Length of the box in x-direction.
	#  @param w Length of the box in y-direction.
	#  @param h Length of the box in z-direction.
	def setsize(self,l,w,h):
		self.geo.Length = l
		self.geo.Width = w
		self.geo.Height = h

	## Copy this box to get a new object with the same properties.
	#  @param newname Name of the new object given as string.
	def copy(self,newname):
		newobject = RBox(newname,self.doc,self.geo.Length,self.geo.Width,self.geo.Height)
		self.copyattributes(newobject)
		return newobject
    

# run with 
#   FreeCAD chamber.py


# From package freecadobjects to be used with python FreeCAD programming.
# Example chamber.py showing how to create a technical drawing.
#
# Copyright (C) 2017 Ronja Hetzel <ronja.hetzel@rwth-aachen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.



import FreeCAD 
import FreeCADGui
import Draft
import math

import sys
sys.path.append("..")
import freecadobjects as FCO


### New Document

docname = "myDocument"
document = FCO.RDocument(docname)


### Set Length (in mm)

pipeFBl = 300 		# length of vacuum pipe
pipeFBri = 200 		# inner radius of this pipe
pipeFBro = 210 		# outer radius of this pipe
ringl = 15 		# thickness of flange ring of the pipe
ringro = pipeFBro+20 	# outer radius of pipe flange (inner is the same as for pipe)

holeradius = 3              # radius of holes for screws 
holelaneradius = ringro-10  # radius on the flange on which the centers of the screw holes are placed
numberofscrewholes = 24     # number of screwholes per ring


### Build Pipe

pipeFB = FCO.RPipe("PipeFB",document,pipeFBri,pipeFBro,pipeFBl)
ringFB1 = FCO.RPipe("RingFB1",document,pipeFBri,ringro,ringl)
ringFB2 = FCO.RPipe("RingFB2",document,pipeFBri,ringro,ringl)

for n in range(numberofscrewholes):
    hole = FCO.RCylinder("hole_"+ringFB1.name[:7]+"_"+str(n),document,holeradius,ringl)
    alpha = n*360.0/numberofscrewholes
    x = holelaneradius*math.sin(math.radians(alpha))
    y = holelaneradius*math.cos(math.radians(alpha))
    hole.translate(FreeCAD.Vector(x,y,0))
    ringFB1 = FCO.RCut(ringFB1.name[:7]+"_"+str(n),ringFB1,hole)

for n in range(numberofscrewholes):
    hole = FCO.RCylinder("hole_"+ringFB2.name[:7]+"_"+str(n),document,holeradius,ringl)
    alpha = n*360.0/numberofscrewholes
    x = holelaneradius*math.sin(math.radians(alpha))
    y = holelaneradius*math.cos(math.radians(alpha))
    hole.translate(FreeCAD.Vector(x,y,0))
    ringFB2 = FCO.RCut(ringFB2.name[:7]+"_"+str(n),ringFB2,hole)

ringFB1.translate(pipeFB.referencepoint+FreeCAD.Vector(0.0,0.0,pipeFBl/2+ringl/2))
ringFB2.translate(pipeFB.referencepoint+FreeCAD.Vector(0.0,0.0,-pipeFBl/2-ringl/2))

pipeFB = FCO.RFuse("PipeFB_1",pipeFB,ringFB1)
pipeFB = FCO.RFuse("PipeFB_2",pipeFB,ringFB2)

document.recompute()


### Activate a specific workench and center view

FreeCADGui.activateWorkbench("PartWorkbench")
FreeCADGui.SendMsgToActiveView("ViewFit")


### Make 2D technical drawing of pipe 

page = FCO.RFeaturePage("Page",document,"A3_Landscape")
page.seteditabletexts("","01.01.2000","","","1:X","","1.0","1/1","Vacuum Pipe","")
page.textauthor = "Erika Musterfrau"
page.fillinfobox()

FCO.RView("FrontView",page,pipeFB,(0.0,0.0,1.0),110.0,95.0,0.3,00.0)
FCO.RView("SideView",page,pipeFB,(0.0,1.0,0.0),110.0,225.0,0.3,-90.0)
FCO.RView("InclinedView",page,pipeFB,(-10.0,4.0,3.0),300.0,110.0,0.3,00.0)

FreeCAD.Console.PrintMessage("Click on 'Page' to see the technical drawing.\n")

document.recompute()

# run with 
#	FreeCAD vetodetector.py


# From package freecadobjects to be used with python FreeCAD programming.
# Example vetodetector.py showing a construction.
#
# Copyright (C) 2017 Ronja Hetzel <ronja.hetzel@rwth-aachen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.



import FreeCAD
import FreeCADGui
import math
import numpy as np

import sys
sys.path.append("..")
import freecadobjects as FCO


lightblue = (0.56,0.73,0.90)
black = (0.,0.,0.)
gray = (0.78,0.78,0.78)

def buildVacuumPipe(document):

    pipe_r_o = 120./2.
    pipe_r_i = 100./2.
    pipe_l = 200

    # Pipe
    pipe_out_x = FCO.RCylinder("POx",document,pipe_r_o,pipe_l)
    pipe_out_y = FCO.RCylinder("POy",document,pipe_r_o,pipe_l)
    pipe_out_z = FCO.RCylinder("POz",document,pipe_r_o,pipe_l)
    
    pipe_out_x.setrotation(FreeCAD.Rotation(FreeCAD.Vector(0,1,0),90))
    pipe_out_y.setrotation(FreeCAD.Rotation(FreeCAD.Vector(1,0,0),90))
    
    pipe_out_temp = FCO.RFuse("POt",pipe_out_x,pipe_out_y) 
    pipe_out = FCO.RFuse("PO",pipe_out_temp,pipe_out_z) 
    
    pipe_in_x = FCO.RCylinder("PIx",document,pipe_r_i,pipe_l)
    pipe_in_y = FCO.RCylinder("PIy",document,pipe_r_i,pipe_l)
    pipe_in_z = FCO.RCylinder("PIz",document,pipe_r_i,pipe_l)
   
    pipe_in_x.setrotation(FreeCAD.Rotation(FreeCAD.Vector(0,1,0),90))
    pipe_in_y.setrotation(FreeCAD.Rotation(FreeCAD.Vector(1,0,0),90))
   
    pipe_in_temp = FCO.RFuse("PIt",pipe_in_x,pipe_in_y) 
    pipe_in = FCO.RFuse("PI",pipe_in_temp,pipe_in_z) 
   
    pipe = FCO.RCut("Pipe",pipe_out,pipe_in)
    pipe.settransparency(90)
    pipe.vis.LineColor = gray
    pipe.vis.PointColor = gray


    
def buildVeto(document):

    sc_x = 50.
    sc_y = 50.*np.sqrt(2.)+50.
    sc_z = 5.

    cut_l = sc_x*np.sqrt(2.)

    # gab in z-direction, must be more than sc_z/2., otherwise the bars overlap
    sc_gap = 4.
    
    # set hole here!
    sc_off = 30.
    
    # shift of center of rotated scintillator
    shift_a = np.sqrt(2.)*(sc_x+sc_y)/4.
    shift_b = np.sqrt(2.)*(sc_y-sc_x)/4.
    
    # \ left
    scb1 = FCO.RBox("ScintiBox1",document,sc_x,sc_y,sc_z)
    scb1.setrotation(FreeCAD.Rotation(FreeCAD.Vector(0,0,1),45))
    #cut edge
    cut1 = FCO.RBox("ScintiCut1",document,cut_l,cut_l,sc_z)
    cut1.translate(FreeCAD.Vector(shift_a-cut_l/2.,-shift_b-cut_l/2.,0))
    sc1 = FCO.RCut("Scinti1",scb1,cut1)
    #default position = closed hole
    sc1.translate(FreeCAD.Vector(shift_a-sc_x*np.sqrt(2),-shift_b,-sc_gap))
    #create given hole
    sc1.translate(FreeCAD.Vector(-sc_off/2.,0,0))
    sc1.setcolor(lightblue)
    
    # / left
    scb2 = FCO.RBox("ScintiBox2",document,sc_x,sc_y,sc_z)
    scb2.setrotation(FreeCAD.Rotation(FreeCAD.Vector(0,0,1),-45))
    #cut edge
    cut2 = FCO.RBox("ScintiCut2",document,cut_l,cut_l,sc_z)
    cut2.translate(FreeCAD.Vector(shift_a-cut_l/2.,+shift_b+cut_l/2.,0))
    sc2 = FCO.RCut("Scinti2",scb2,cut2)
    #default position = closed hole
    sc2.translate(FreeCAD.Vector(shift_a-sc_x*np.sqrt(2),shift_b,sc_gap))
    #create given hole
    sc2.translate(FreeCAD.Vector(-sc_off/2.,0,0))
    sc2.setcolor(lightblue)
    
    # \ right
    scb3 = FCO.RBox("ScintiBox3",document,sc_x,sc_y,sc_z)
    scb3.setrotation(FreeCAD.Rotation(FreeCAD.Vector(0,0,1),45))
    #cut edge
    cut3 = FCO.RBox("ScintiCut3",document,cut_l,cut_l,sc_z)
    cut3.translate(FreeCAD.Vector(-shift_a+cut_l/2.,+shift_b+cut_l/2.,0))
    sc3 = FCO.RCut("Scinti3",scb3,cut3)
    #default position = closed hole
    sc3.translate(FreeCAD.Vector(-shift_a+sc_x*np.sqrt(2),shift_b,-sc_gap))
    #create given hole
    sc3.translate(FreeCAD.Vector(sc_off/2.,0,0))
    sc3.setcolor(lightblue)
    
    # / right
    scb4 = FCO.RBox("ScintiBox4",document,sc_x,sc_y,sc_z)
    scb4.setrotation(FreeCAD.Rotation(FreeCAD.Vector(0,0,1),-45))
    #cut edge
    cut4 = FCO.RBox("ScintiCut4",document,cut_l,cut_l,sc_z)
    cut4.translate(FreeCAD.Vector(-shift_a+cut_l/2.,-shift_b-cut_l/2.,0))
    sc4 = FCO.RCut("Scinti4",scb4,cut4)
    #default position = closed hole
    sc4.translate(FreeCAD.Vector(-shift_a+sc_x*np.sqrt(2),-shift_b,sc_gap))
    #create given hole
    sc4.translate(FreeCAD.Vector(sc_off/2.,0,0))
    sc4.setcolor(lightblue)
    
    
    rod_r = 5
    rod_overlap = 5
    pipe_l = 200
    rod_l = pipe_l/2.-sc_x*np.sqrt(2)-sc_off/2.+rod_overlap
    
    # left
    rod1 = FCO.RCylinder("Screw1",document,rod_r,rod_l)
    rod1.setrotation(FreeCAD.Rotation(FreeCAD.Vector(0,1,0),90))
    rod1.translate(FreeCAD.Vector(-sc_x*np.sqrt(2)-rod_l/2.+rod_overlap,0,0))
    #create given hole
    rod1.translate(FreeCAD.Vector(-sc_off/2.,0,0))
    rod1.setcolor(black)
    
    #right
    rod2 = FCO.RCylinder("Screw2",document,rod_r,rod_l)
    rod2.setrotation(FreeCAD.Rotation(FreeCAD.Vector(0,1,0),90))
    rod2.translate(FreeCAD.Vector(sc_x*np.sqrt(2)+rod_l/2.-rod_overlap,0,0))
    #create given hole
    rod2.translate(FreeCAD.Vector(sc_off/2.,0,0))
    rod2.setcolor(black)
    



##### MAIN #####

### Create a document
documentname = "myDocument"
document = FCO.RDocument(documentname)


### Construct the vacuum pipe and the veto detector
buildVacuumPipe(document)
buildVeto(document)


### Activate a specific workench
FreeCADGui.activateWorkbench("PartWorkbench")


### Set the direction of view
from pivy import coin
cam = FreeCADGui.ActiveDocument.ActiveView.getCameraNode()
rot = coin.SbRotation()
rot.setValue(coin.SbVec3f(0,1,0),math.pi/4)
nrot = cam.orientation.getValue() * rot
cam.orientation = nrot 
rot.setValue(coin.SbVec3f(1,0,-1),-math.pi/8)
nrot = cam.orientation.getValue() * rot
cam.orientation = nrot 


### Fit the whole setup to the screen
FreeCADGui.SendMsgToActiveView("ViewFit")


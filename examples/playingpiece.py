# run with 
#   FreeCAD playingpiece.py


# From package freecadobjects to be used with python FreeCAD programming.
# Example playingpiece.py showing a basic construction.
#
# Copyright (C) 2017 Ronja Hetzel <ronja.hetzel@rwth-aachen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.



import FreeCAD 
import FreeCADGui
import Draft
import math

import sys
sys.path.append("..")
import freecadobjects as FCO


### New Document

docname = "myDocument"
document = FCO.RDocument(docname)


### Set Length (in mm)

l_board = 50
r_cone = 5
h_cone = 15
r_sphere = 3


### Build Figure

board = FCO.RPlane("board",document,l_board,l_board)

body = FCO.RCone("body",document,r_cone,0,h_cone)
head = FCO.RSphere("head",document,r_sphere)

body.translate(FreeCAD.Vector(1,2,5))
body.setrotation(FreeCAD.Rotation(FreeCAD.Vector(1,1,0),45))

head.translate(body.getTipPosition())

piece = FCO.RFuse("piece",body,head)



### Activate a specific workench and center view

FreeCADGui.activateWorkbench("PartWorkbench")
FreeCADGui.SendMsgToActiveView("ViewFit")
FreeCADGui.activeDocument().activeView().viewFront()

# FreeCAD Objects


This project defines some own classes for **FreeCAD** Objects to be used in **python** scripts.


The package is useful for not so complex geometries, which consist of the predefined FreeCAD Parts.
You can still use all normal python FreeCAD commands in your script.
After running you program with FreeCAD, the FreeCAD Gui will open and you can continue working on the construction using all functions of the Gui.

It is an add-on to FreeCAD, a general purpose parametric 3D CAD modeler (<https://www.freecadweb.org>).
FreeCAD is developed by 
Juergen Riegel, Werner Mayer, Yorik van Havre 2001-2015
and published under Lesser General Public Licence, version 2 or superior (LGPL2+).

The package is tested on FreeCAD version 0.13 and 0.16

You find the **documentation** of the freecadobject package here:
<http://ronja.hetzel.pages.rwth-aachen.de/FreeCADObjects/>

---

Copyright (C) 2017 Ronja Hetzel <ronja.hetzel@rwth-aachen.de>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
